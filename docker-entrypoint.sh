#!/bin/bash
set -e

if [[ -z $1 ]];then
  if [[ -n ${SSL_HOST} ]];then
    python xmledit.py -u '//Service[@name="Catalina"]/Connector[@port="8080"]' "proxyName=${SSL_HOST}"\
                                                                               "proxyPort=${SSL_PORT:-443}"\
                                                                               'secure=true'\
                                                                               'scheme=https'\
                      -f /app/conf/server.xml --clean
  fi
  gosu java /app/bin/start-jira.sh -fg
else
  exec $@
fi
